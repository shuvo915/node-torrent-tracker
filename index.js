var fs      = require('fs');
var tracker = require('./tracker');
var http    = require('./http');

fs.readFile(__dirname + '/config.json', 'utf8', function (error, data) {
	if (error) {
		console.log('Error: ' + error);
		return;
	}

  	config = JSON.parse(data);

  	var trackerInstance = new tracker.Tracker();
  	trackerInstance.setup(config.tracker);
  	trackerInstance.start();

	http.createServer(trackerInstance, config.http.port, config.http.host);
});