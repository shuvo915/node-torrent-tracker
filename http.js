var http 		= require('http');
var url 		= require('url');
var querystring = require('querystring');
var tracker 	= require('./tracker');

const FAILURE_REASONS = {
	100: "Invalid request type: client request was not a HTTP GET",
	101: "Missing info_hash",
	102: "Missing peer_id",
	103: "Missing port",
	150: "Invalid infohash: infohash is not 20 bytes long",
	151: "Invalid peerid: peerid is not 20 bytes long",
	152: "Invalid numwant. Client requested more peers than allowed by tracker",
	200: "info_hash not found in the database.",
	500: "Client sent an eventless request before the specified time",
	900: "Generic error",
	404: "Requested Resource Not Found"
  };

function parse(qs, sep, eq) {
  sep = sep || "&";
  eq = eq || "=";
  var obj = {};

  if (typeof qs !== 'string') {
    return obj;
  }

  qs.split(sep).forEach(function(kvp) {
    var x = kvp.split(eq);
    var k = x[0];
    var v = x.slice(1).join(eq);
	//var v = x[1];
    if (!(k in obj)) {
        obj[k] = v;
    } else if (!Array.isArray(obj[k])) {
        obj[k] = [obj[k], v];
    } else {
        obj[k].push(v);
    }
  });

  return obj;
};

function Failure(code, reason) {
	this.code   = code;
	this.reason = reason;
	if (reason == undefined && typeof FAILURE_REASONS[this.code] != "undefined")
		this.reason = FAILURE_REASONS[this.code]
	else if (this.code == null)
		this.code = 900;
  }

Failure.prototype = {
	bencode: function() {
		return "d14:failure reason"+ this.reason.length +":"+ this.reason +"12:failure codei"+ this.code +"ee"
	}
  }

const PARAMS_INTEGER = [ 'port', 'uploaded', 'downloaded', 'left', 'compact', 'numwant' ];
const PARAMS_STRING  = [ 'event' ];

function processRequest(request) {
	var uri   = url.parse( request.url );
	var query = parse( uri.query );

	if (request.method != "GET")
		throw new Failure(100);

	var matches   = /^\/?(\w{32}?)\/(announce|scrape|status)\/?$/gi.exec(uri.pathname);

	if (matches == null)
		throw new Failure(404);

	query.passkey = matches[1];
	query.request = matches[2];

	if (typeof query.info_hash == 'undefined')
		throw new Failure(101);
	query._info_hash = querystring.unescapeBuffer( query.info_hash );
	query.info_hash =  tracker.bufferHex( query._info_hash );
	if (typeof query.peer_id == 'undefined')
		throw new Failure(102);
	query._peer_id = querystring.unescapeBuffer( query.peer_id );
	query.peer_id  =  tracker.bufferHex( query._peer_id );

	if (typeof query.port == 'undefined')
		throw new Failure(103);

	if (query.info_hash.length != 40)
		throw new Failure(150);

	if (query.peer_id.length > 50)
		throw new Failure(151);

	if (query.ip == undefined)
		query.ip = request.connection.remoteAddress;

	if (typeof query.numwant == 'undefined' || !(query.numwant > 0) )
		query.numwant = 50;

	if (typeof query.event == 'undefined' )
		query.event = 0;

	for (var i = 0; i < PARAMS_INTEGER.length; i++) {
		var p = PARAMS_INTEGER[i];
		if (typeof query[p] != undefined) {
			query[p] = parseInt(query[p].toString(), 10);
		}
	}

	for (var i = 0; i < PARAMS_STRING.length; i++) {
		var p = PARAMS_STRING[i];
		if (typeof query[p] != undefined) {
			query[p] = query[p].toString();
		}
	}

	return query;
  }

function announce(trackerInstance, query, response) {
	var sendResponse 	= function(torrent, peer) {

		var peerBuffer = new Buffer(query.numwant * tracker.PEER_COMPACT_SIZE );
		var len        = torrent.writePeers(peerBuffer, query.numwant, peer);
		peerBuffer     = peerBuffer.slice(0, len);

		var resp = "d8:intervali"+ tracker.ANNOUNCE_INTERVAL +"e8:completei"+ torrent.seeders +"e10:incompletei"+ torrent.leechers +"e10:downloadedi"+ torrent.times_completed +"e5:peers"+ len +":";

		response.writeHead(200, {
			'Content-Length': resp.length + peerBuffer.length + 1,
			'Content-Type': 'text/plain'
		});

		response.write(resp);
		response.write(peerBuffer);
		response.end("e");
	};

	var onTorrentFound 	= function(torrent) {
		if ( !(torrent instanceof  tracker.Torrent) )
			throw new Failure(900, 'INTERNAL ERROR');
		else if ( torrent  == 'NOT FOUND' )
			throw new Failure(200)

		if (false) { /* CHECK USER PERMISSION */

		}

		query.torrent_id = torrent.id;
		var peer = torrent.addPeer(query);
		sendResponse(torrent, peer);
	};

	var onUserFound 	= function(user) {
		if ( !(user instanceof  tracker.User) )
			throw new Failure(900, 'INTERNAL ERROR');
		else if ( user == 'NOT FOUND' )
			throw new Failure(900, 'Passkey not registered. Contact Adminstrator')

		if (false) { /* CHECK USER PERMISSION */

		}
		query.user_id = user.id;
		trackerInstance.getTorrent(query.info_hash, onTorrentFound);
	};

	trackerInstance.getUser(query.passkey, onUserFound);
  }

function createServer(trackerInstance, port, host) {
	port = port || 8080;
	host = host || '127.0.0.1';

	var server = http.createServer(function(request, response)
	  {
		try {
			var params = processRequest( request );

			switch( params.request ) {
				case 'announce':
					console.log('announce')
					announce(trackerInstance, params, response);
				break;

				case 'scrape':
				break;
			}

		} catch (error) {
			console.log(error)
			var resp = "Internal Error";
			if (!(error instanceof Failure))
				resp = new Failure(900, "Internal Error");

			if (error.code == 404) {
				resp = 'Resource Not Found';
				response.writeHead(404, {
					'Content-Length': resp.length,
					'Content-Type': 'text/plain'
				});
				response.end( resp );
			} else {

				if (error != undefined) {
				 	resp = error.bencode;
				}
				response.writeHead(500, {
					'Content-Length': resp.length,
					'Content-Type': 'text/plain'
				});
				response.end(resp);
			}
		}

	  }).listen(port, host, function() {
			var address = server.address();
			console.log('TorrentBD Tracker - HTTP server listening ', address.address, ':', address.port);
	  });
  }

exports.createServer = createServer;