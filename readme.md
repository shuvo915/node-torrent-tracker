Node Bittorrent Tracker
=======================
This is a private torrent tracker, still in it's early stage utilizing [NODE.JS](http://nodejs.org/ NODE.JS). It uses MySql as database to store user, torrents and peers.

###Requirements
1. MySql Server
2. <https://github.com/felixge/node-mysql> (The MySql client used. Installation required)

###Instructions
Just run `node index.js`
<br/>
To configure some basic settings edit `config.json`
<br/>
P.S. You need <https://github.com/felixge/node-mysql> installed

###Motivation
I needed to customize the torrent tracker on my site which is running [XBT Tracker](http://xbtt.sourceforge.net/tracker/ XBT Tracker). But I am not proficient in C++ and couldn't undestand the code. So I thought of writting a new one in javascript after hearing about [NODE.JS](http://nodejs.org/ NODE.JS). There wasn't any private torrent tracker in NODE so I set out to write my own.
<br/>
I took help from the followig projects written for NODE
<br/>
<https://github.com/WizKid/node-bittorrent-tracker>
<br/>
<https://github.com/anhel/node-bittorrent-tracker>

###TODO
1. Implement scrape
2. Conform better to Bittorrent Specifications
3. Implement features like SeedBonus, Hit And Run etc.
4. Real Time Stats and Monitoring
5. Optimize code, Better error handling, Better Load Handling
6. Cleanup debug code
7. More configurable settings

######I am really hoping for someone to help me finish this. This is my first project in node not to mention in open source. 