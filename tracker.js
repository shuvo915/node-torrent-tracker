var mysql     = require('mysql');
var dbConfig  = {};
var connection;

const PEER_COMPACT_SIZE = 6;

const ANNOUNCE_INTERVAL = 60;

function now() {
  return Math.floor(new Date().getTime()/1000);
}

function bufferHex (b) {
    var s = '';
    for (var i = 0; i < b.length; i++) {
      if(b[i] < 16)
          s+='0';
      s += b[i].toString(16);
    }
     return s;
}
function handleDisconnect() {
    if ( dbConfig == undefined ) {
        dbConfig = {
            host     : '127.0.0.1',
            user     : 'root',
            password : '',
            database : 'welpserv_xeno',
        };
    }

  	var conn = mysql.createConnection(dbConfig); // Recreate the connection, since
                                             // the old one cannot be reused.

  	conn.connect(function(error) {
  		  console.log('Connected To db');         // The server is either down
      	if(error) {                             // or restarting (takes a while sometimes).
        		console.log('error when connecting to db:', error);
        		setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
      	}                                     	// to avoid a hot loop, and to allow our node script to
  	});                                     	  // process asynchronous requests in the meantime.
                                          		  // If you're also serving http, display a 503 error.
  	conn.on('error', function(error) {
      	console.log('db error', error);
      	if(error.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
        		handleDisconnect();                         // lost due to either server restart, or a
      	} else {                                        // connnection idle timeout (the wait_timeout
        		throw error;                                // server variable configures this)
      	}
  	});
    return conn;
};

const EVENT_NONE = 0;
const EVENT_COMPLETED = 1;
const EVENT_STARTED = 2;
const EVENT_STOPPED = 3;

function event(e) {
  switch (e) {
    case "completed":
      return EVENT_COMPLETED;
    case "started":
      return EVENT_STARTED;
    case "stopped":
      return EVENT_STOPPED;
  }

  return EVENT_NONE;
}

const PEERSTATE_SEEDER  = 0;
const PEERSTATE_LEECHER = 1;

function Peer(peer_id, user_id, infohash, torrent_id) {
    if (!(this instanceof Peer)) {
        return new Peer(peer_id, user_id, infohash, torrent_id);
    }

    this._peer_id    = peer_id;
    this.peer_id    = bufferHex( peer_id );
    this.user_id    = user_id;
    this.infohash   = infohash;
    this.torrent_id = torrent_id;

    this.state      = -1;
    this.event      = -1;

    this._downloaded = 0;
    this._uploaded   = 0;

    this.downloaded = 0;
    this.uploaded   = 0;
    this.remaining  = 0;

    this.ip         = null;
    this.port       = 0;

    this.started    = null;
    this.lastAction = null;

    this.compact    = null;
}

Peer.prototype = {
      timedOut: function(n) {
        return n - this.lastAction.getTime()/1000 > ANNOUNCE_INTERVAL * 2;
      },
      touch: function() {
        this.lastAction = new Date();
      },
      _compact : function(ip, port)
        {
          var b = new Buffer(PEER_COMPACT_SIZE);

          var parts = ip.split(".");
          if (parts.length != 4)
            throw 1

          for (var i = 0; i < 4; i++)
            b[i] = parseInt(parts[i]);

          b[4] = (port >> 8) & 0xff;
          b[5] = port & 0xff;

          return b;
        }
  };

function Torrent(id) {
    if (!(this instanceof Torrent)) {
        return new Torrent(id);
    }

    this.id               = id;
    this.times_completed  = 0;
    this.seeders          = 0;
    this.leechers         = 0;

    this.peerList  = [];
    this._peerList = {};
}

Torrent.prototype = {
    addPeer: function(params) {
      var me = this;
      var n  = now();
      var i  = 0;
      var newPeerDict = {};
      var newPeerList = [];
      /* Cleanup List if Necessary */
      if (  me.seeders + me.leechers < me._peerList.length / 2
          && this.peerList.length > 10)
            {
              for (var p in me._peerList) {
                  if (!me._peerList.hasOwnProperty(p))
                    continue;

                  var tempPeer = me._peerList[p];

                  if ( tempPeer.timedOut(n) ) { /* TODO */
                    if (tmpPeer.state == PEERSTATE_LEECHER)
                      this.leechers--;
                    else
                      this.seeders--;
                    continue;
                  }

                  newPeerList[i] = tempPeer;
                  newPeerDict[p] = i;
                  i++;
              }

              me.peerList = newPeerList;
              //this.lastCompact = n;
            }

      var peer = new Peer(params._peer_id, params.user_id, params.info_hash, params.torrent_id);
      peer.event = event(params.event);
      peer.passkey     = params.passkey;
      peer._downloaded = params._downloaded;
      peer._uploaded   = params._uploaded;
      peer.downloaded  = params.downloaded;
      peer.uploaded    = params.uploaded;
      peer.remaining   = params.left;
      peer.ip          = params.ip;
      peer.started     = new Date();
      peer.port        = params.port;
      peer.compact     = peer._compact( peer.ip, peer.port );
      peer.touch();

      if (peer.event == EVENT_COMPLETED && peer.state == PEERSTATE_SEEDER)
          me.times_completed++;

      if ( me._peerList.hasOwnProperty(peer.peer_id) ) {
          var oldPeer = me._peerList[peer.peer_id];

          if (peer.event == EVENT_STOPPED) {
            if (oldPeer.state === PEERSTATE_LEECHER)
              this.leechers--;
            else
              this.seeders--;

              if ( newPeerDict.hasOwnProperty(peer.peer_id) ) {
                  var index = newPeerDict[peer.peer_id];
                  delete newPeerDict[peer.peer_id];
                  delete newPeerList[ index ];
              }
          } else {

            if (oldPeer.state != peer.state) {
              if (peer.state === PEERSTATE_LEECHER) {
                this.leechers++;
                this.seeders--;
              } else {
                this.leechers--;
                this.seeders++;
              }

              oldPeer.state = peer.state;
            }
          }

          oldPeer.uploaded   = peer.uploaded;
          oldPeer.downloaded = peer.downloaded;
          oldPeer.remaining  = peer.remaining;
          oldPeer.ip         = peer.ip;
          oldPeer.port       = peer.port;
          oldPeer.compact    = oldPeer._compact(oldPeer.ip, oldPeer.port);
          oldPeer.touch();

          peer = oldPeer;
        }
      else if (peer.event != EVENT_STOPPED) {
          me._peerList[peer.peer_id] = peer;
          newPeerList.push(peer);

          if (peer.state === PEERSTATE_LEECHER)
            this.leechers++;
          else
            this.seeders++;

          var sql     = 'INSERT ?? (??) VALUES(?);'
          var escapes = [ 'peers', ['peer_id', 'uploaded', 'downloaded', 'to_go', 'ip', 'port', 'started', 'last_action', 'seeder', 'torrent', 'userid', 'passkey'],
                          [ peer._peer_id, peer.uploaded, peer.downloaded, peer.remaining, peer.ip, peer.port, peer.started, peer.lastAction, peer.state == PEERSTATE_SEEDER? 'yes': 'no', params.torrent_id, params.user_id, params.passkey ] ];
          var addNewPeer = function(error, result)
            {
                if (error) {
                  console.log( error )
                  return;
                }
                peer.id = result.insertId;
                console.log('INSERTED NEW PEER');
            };
          var query = connection.query(sql, escapes, addNewPeer);
          console.log( query.sql );
        }

      me.peerList = newPeerList;
    },
    writePeers: function(b, count, selfPeer) {
      var c = 0;
      if (count > this.seeders + this.leechers) {
        for (var i = this.peerList.length - 1; i >= 0; i--) {
          var p = this.peerList[i];
          if (p != undefined && p != selfPeer)
            p.compact.copy(b, c++ * PEER_COMPACT_SIZE);
        }
      } else {
        var m = Math.min(this.peerList.length, count);
        for (var i = 0; i < m; i++) {
          var index = Math.floor(Math.random() * this.peerList.length);
          var p = this.peerList[index];
          if (p != undefined && p != selfPeer)
            p.compact.copy(b, c++ * PEER_COMPACT_SIZE);
        }
      }

      return c * PEER_COMPACT_SIZE;
    },
  };

function User(id) {
    if (!(this instanceof User)) {
        return new User(id);
    }
    this.id            = id;
    this.downloaded    = 0;
    this.uploaded      = 0;
    this.allowDownload = true;
}

function Tracker() {
    if (!(this instanceof Tracker)) {
        return new Tracker();
    }

    this.torrents   = {};
    this.users      = {};

    this.lastCommit = null;
}

Tracker.prototype = {
    start: function() {
        connection = handleDisconnect();
        this.intervalID = setInterval(commit, 10000, this);
        console.log('Tracker Commit Process Started ');
      },
    setup : function(config) {
        dbConfig.host     = config.host     || '127.0.0.1';
        dbConfig.user     = config.user     || 'root';
        dbConfig.password = config.password || '';
        dbConfig.database = config.database || 'welpserv_xeno';
        this.commit_interval = (config.commit_interval || 30*60)*1000;
      },
    getTorrent : function(infohash, torrentCallback) {
        var me = this;
        if ( this.torrents.hasOwnProperty( infohash ) ) {
            torrentCallback( this.torrents[infohash] );
            return;
        }

        console.log('Fetching Torrent From DB : ', infohash);

        var sql = 'SELECT ??, ??, ??, ??, ?? FROM ?? WHERE ?? = ? LIMIT 1;';
        var params = [ 'id', 'info_hash', 'times_completed', 'seeders', 'leechers', 'torrents', 'info_hash', infohash ];
        var resultCallback = function(error, rows, fields)
          {
              if ( error ) {
                torrentCallback(error);
                return;
              }
              if ( rows == undefined || rows.length < 1) {
                torrentCallback('NOT FOUND');
                return;
              }

              var torrent = new Torrent(rows[0].id);
              torrent.times_completed = rows[0].times_completed;
              torrent.seeders         = rows[0].seeders;
              torrent.leechers        = rows[0].leechers;
              me.torrents[infohash]   = torrent;

              var sql    = 'SELECT * FROM ?? WHERE ?? = ?';
              var params = ['peers', 'torrent', torrent.id];
              debugger;
              var query  = connection.query(sql, params, function onPeerFound(error, rows, fields)
                {
                  if ( error || rows == undefined || !(rows.length > 0) ) {
                    torrentCallback(torrent);
                    return;
                  }

                  for (var i = 0; i < rows.length; i++) {
                      var row   = rows[i];
                      var peer  = new Peer( row.peer_id, row.userid, row.info_hash, row.torrent);
                      peer.id          = row.id;
                      peer.passkey     = row.passkey;
                      peer._downloaded = peer.downloaded = row.downloaded;
                      peer._uploaded   = peer.uploaded   = row.uploaded;
                      peer.remaining   = row.to_go;
                      peer.ip          = row.ip;
                      peer.port        = row.port;
                      peer.started     = row.started;
                      peer.lastAction  = row.last_action;
                      peer.state       = row.seeder == 'yes'? PEERSTATE_SEEDER : PEERSTATE_LEECHER;
                      peer.state       = EVENT_NONE;
                      peer.compact     = peer._compact( peer.ip, peer.port );

                      torrent._peerList[peer.peer_id] = peer;
                      torrent.peerList.push( peer );
                  };

                  torrentCallback( torrent );
                });

              console.log( query.sql );
          }

        var query = connection.query(sql, params, resultCallback);
        console.log( query.sql );
      },
    getUser : function(passkey, userCallback) {
        var me = this;
        if ( me.users.hasOwnProperty( passkey ) ) {
            userCallback( me.users[passkey] );
            return;
        }

        console.log('Fetching User From DB : ', passkey);

        var sql = 'SELECT ??, ??, ??, ??, ?? FROM ?? WHERE ?? = ? LIMIT 1;';
        var params = [ 'id', 'passkey', 'downloaded', 'uploaded', 'downloadbanned','users', 'passkey', passkey ];
        var resultCallback = function(error, rows, fields) {
            if ( error ) {
              userCallback(error);
              return;
            }
            if ( rows == undefined || rows.length < 1) {
              userCallback('NOT FOUND');
              return;
            }

            var user = new User(rows[0].id);
            user.downloaded    = rows[0].downloaded;
            user.uploaded      = rows[0].uploaded;
            user.allowDownload = rows[0].downloadbanned != 'yes';

            me.users[passkey] = user;
            userCallback(user);
          }

        var query = connection.query(sql, params, resultCallback);
        console.log( query.sql );
      }
};

var commit = function(tracker) {
        var me = tracker;
        var conn;

        var createUserTemp = function(error, result)
          {
              console.log('Temporary User Table Created. ', error);
              if (error) {
                  conn.rollback(function() {
                      throw error; /* TODOD */
                  });
              }

              var sql    = 'INSERT ?? VALUES ?;';
              var params = [ 'users_temp', userInsertParams ];
              connQuery = conn.query( sql, params, insertUserTemp );
              console.log(connQuery.sql);

          };

        var insertUserTemp = function(error, result)
          {
              console.log('Temporary Users Inserted. ', error);
              if (error) {
                  conn.rollback(function() {
                      throw error; /* TODOD */
                  });
              }

                var sql    = 'UPDATE ?? INNER JOIN ?? ON ??.?? = ??.?? SET ??.?? = ??.?? + ??.??, ??.?? = ??.?? + ??.??;';
                var params = [ 'users', 'users_temp',
                              'users', 'id', 'users_temp', 'id',
                              'users', 'downloaded', 'users', 'downloaded', 'users_temp', 'downloaded',
                              'users', 'uploaded', 'users', 'uploaded', 'users_temp', 'uploaded' ];
              connQuery = conn.query( sql, params, updateUserTemp );
              console.log(connQuery.sql);
          };

        var updateUserTemp = function(error, result)
          {
              console.log(connQuery.sql);
              console.log('Temporary Users Updated. ', error);
              if (error) {
                  conn.rollback(function() {
                      throw error; /* TODOD */
                  });
              }
              if (torrentInsertParams.length > 0) {
                var sql    = 'CREATE TEMPORARY TABLE ?? (?? INT, ?? INT, ?? INT, ?? INT);';
                var params = [ 'torrents_temp', 'id', 'times_completed', 'seeders', 'leechers' ];
                connQuery = conn.query(sql, params, createTorrentsTemp);
              } else {
                conn.commit(function(error)
                  {
                    if (error) {
                        conn.rollback(function() {
                            throw error;
                        });
                    }
                    me.lastCommit = now();
                    var d = new Date();
                    console.log( 'Changes Committed To database at ',  d.toDateString(), ' ', d.toTimeString() );
                  });
              }
          };

        var createTorrentsTemp = function(error, result)
            {
                console.log('Temporary Torrents Table Created. ', error);
                if (error) {
                    conn.rollback(function() {
                        throw error; /* TODOD */
                    });
                }

                var sql    = 'INSERT ?? VALUES ?;';
                var params = [ 'torrents_temp', torrentInsertParams ];

                connQuery = conn.query( sql, params, insertTorrentsTemp );
                console.log(connQuery.sql);
            };

        var insertTorrentsTemp = function(error, result)
            {
                console.log('Temporary Torrents Inserted. ', error);
                if (error) {
                    conn.rollback(function() {
                        throw error; /* TODOD */
                    });
                }

                var sql    = 'UPDATE ?? INNER JOIN ?? ON ??.?? = ??.?? SET ??.?? = ??.??, ??.?? = ??.??, ??.?? = ??.??;';
                var params = [ 'torrents', 'torrents_temp',
                              'torrents', 'id', 'torrents_temp', 'id',
                              'torrents', 'times_completed', 'torrents_temp', 'times_completed',
                              'torrents', 'seeders', 'torrents_temp', 'seeders',
                              'torrents', 'leechers', 'torrents_temp', 'leechers' ];

                connQuery = conn.query( sql, params, updateTorrentsTemp );
                console.log(connQuery.sql);
            };

        var updateTorrentsTemp = function(error, result)
            {
                console.log('Temporary Torrents Updated. ', error);
                if (error) {
                    conn.rollback(function() {
                        throw error; /* TODOD */
                    });
                }

                var sql    = 'CREATE TEMPORARY TABLE ?? (?? INT, ?? BIGINT, ?? BIGINT, ?? BIGINT, ?? VARCHAR(3), ?? VARCHAR(64), ?? SMALLINT, ?? DATETIME);';
                var params = [ 'peers_temp', 'id', 'uploaded', 'downloaded', 'to_go', 'seeder', 'ip', 'port', 'last_action' ];
                connQuery = conn.query(sql, params, createPeersTemp);
                console.log(connQuery.sql);
                // conn.commit(function(error) {
                //     if (error) {
                //         conn.rollback(function() {
                //             throw error;
                //         });
                //     }
                //     me.lastCommit = now();
                //     var d = new Date();
                //     console.log( 'Changes Committed To database at ',  d.toDateString(), ' ', d.toTimeString() );
                // });
            };

        var createPeersTemp = function(error, result)
          {
            console.log('Created Temporary Peers Table. ', error);
            if (error) {
                conn.rollback(function() {
                    throw error; /* TODOD */
                });
            }

            var sql    = 'INSERT ?? VALUES ?;';
            var params = [ 'peers_temp' ];
            params.push(peerInsertParams);
            connQuery = conn.query(sql, params, insertPeersTemp);
            console.log(connQuery.sql);

          };

        var insertPeersTemp = function(error, result)
          {
            console.log('Temporary Peers Inserted. ', error);
            if (error) {
                conn.rollback(function() {
                    throw error; /* TODOD */
                });
            }

            var sql    = 'UPDATE ?? INNER JOIN ?? ON ??.?? =  ??.?? SET ??.?? =  ??.??, ??.?? =  ??.??, ??.?? =  ??.??, ??.?? =  ??.??, ??.?? =  ??.??, ??.?? =  ??.?? ,??.?? =  ??.??;';
            var params = [ 'peers', 'peers_temp', 'peers', 'id', 'peers_temp', 'id',
                              'peers', 'uploaded', 'peers_temp', 'uploaded',
                              'peers', 'downloaded', 'peers_temp', 'downloaded',
                              'peers', 'to_go', 'peers_temp', 'to_go',
                              'peers', 'seeder', 'peers_temp', 'seeder',
                              'peers', 'ip', 'peers_temp', 'ip',
                              'peers', 'port', 'peers_temp', 'port',
                              'peers', 'last_action', 'peers_temp', 'last_action' ];
            connQuery = conn.query(sql, params, updatePeersTemp);
            console.log(connQuery.sql);
          };

        var updatePeersTemp = function(error, result)
          {
            console.log('Temporary Peers Updated. ', error);
            if (error) {
                conn.rollback(function() {
                    throw error; /* TODOD */
                });
            }

            conn.commit(function(error) {
                if (error) {
                    conn.rollback(function() {
                        throw error;
                    });
                }
                me.lastCommit = now();
                var d = new Date();
                console.log( 'Changes Committed To database at ',  d.toDateString(), ' ', d.toTimeString() );
            });
          };

        var userInsertParams    = [];
        var torrentInsertParams = [];
        var peerInsertParams    = [];

        for (var infohash in me.torrents)
          {
            if ( !me.torrents.hasOwnProperty(infohash) )
                continue;

            var torrent = me.torrents[infohash];

            for (var p in torrent._peerList) {
                if (!torrent._peerList.hasOwnProperty(p))
                    continue;

                var peer     = torrent._peerList[p];
                var user     = me.users[peer.passkey];

                if (user instanceof User) {
                  console.log(peer.passkey);
                  console.log(user);
                  user.uploaded += (peer.uploaded - peer._uploaded);

                  if (!torrent.freeleech)
                      user.downloaded += (peer.downloaded - peer._downloaded);
                }
                peer._uploaded   = peer.uploaded;
                peer._downloaded = peer.downloaded;

                peerInsertParams.push([ peer.id, peer.uploaded, peer.downloaded, peer.remaining, peer.ip, peer.port, peer.state == PEERSTATE_SEEDER? 'yes': 'no', peer.lastAction ]);
            };

            torrentInsertParams.push([ torrent.id, torrent.times_completed, torrent.seeders, torrent.leechers]);
          }

        for (var passkey in me.users)
          {
            if ( !me.users.hasOwnProperty(passkey) )
                continue;

            var user = me.users[passkey];

            userInsertParams.push([ user.id, user.downloaded, user.uploaded ]);
            user.uploaded   = 0;
            user.downloaded = 0;
         }

        if (userInsertParams.length < 1 && torrentInsertParams.length < 1)
            return;

        conn = handleDisconnect();
            console.log(userInsertParams)
            console.log(torrentInsertParams)
        conn.beginTransaction( function(error)
          {
            var connQuery;
            console.log('Commit Transaction Started. ', error);
            if (error) {
                conn.rollback(function() {
                    throw error; /* TODOD */
                });
            }
            if (userInsertParams.length > 0) {
                var sql    = 'CREATE TEMPORARY TABLE ?? (?? INT, ?? INT, ?? INT);';
                var params = [ 'users_temp', 'id', 'downloaded', 'uploaded' ];
                connQuery = conn.query(sql, params, createUserTemp);
                console.log(connQuery.sql);
            } else if (torrentInsertParams.length > 0) {
                var sql    = 'CREATE TEMPORARY TABLE ?? (?? INT, ?? INT, ?? INT, ?? INT);';
                var params = [ 'torrents_temp', 'id', 'times_completed', 'seeders', 'leechers' ];
                connQuery = conn.query(sql, params, createTorrentsTemp);
            }

          });

      };
exports.Peer = Peer;
exports.Torrent = Torrent;
exports.User = User;
exports.Tracker = Tracker;
exports.bufferHex = bufferHex;

exports.PEER_COMPACT_SIZE = PEER_COMPACT_SIZE
exports.ANNOUNCE_INTERVAL = ANNOUNCE_INTERVAL;